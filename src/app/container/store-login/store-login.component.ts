import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { SwalComponent } from '@toverux/ngsweetalert2';
import { StoreService } from '../../service/store/store.service';
import { Cookie } from 'ng2-cookies/ng2-cookies';

@Component({
  selector: 'app-store-login',
  templateUrl: './store-login.component.html',
  styleUrls: ['./store-login.component.css'],
  providers: [StoreService]
})
export class StoreLoginComponent implements OnInit {
  @ViewChild('dialogError') private swalDialogError: SwalComponent;
  public storeId: String = 'a001';
  public storePwd: String = '123456';
  public cookie: any;
  constructor(
    private router: Router,
    private storeService: StoreService
  ) { }

  ngOnInit() {
    this.storeCheckCookie();
  }

  /**
   * 檢查是否已有登入紀錄
   */
  public storeCheckCookie() {
    this.cookie = Cookie.get('userCookie');
    if (this.cookie) {
      this.storeGetInfo(); // auto login
    }
  }

  public async storeGetInfo() {
    await this.storeService.storeUserInfo()
      .subscribe(result => {
        result ? this.router.navigate(['/childlogin']) : null;
      }, err => {
        console.log('店家尚未登入')
      })
  }

  /**
   * 提交表單資料
   */
  public async storeSubmit() {
    const body = {
      userId: this.storeId,
      userPwd: this.storePwd
    };
    console.log(body);
    await this.storeService.Login(body)
      .subscribe(
        result => {
          console.log(result);
          if (result.token) {
            Cookie.set('userCookie', result.token, 36500, '/');
            this.storeGetInfo();
          } else {
            this.swalDialogError.show();
          }
        });
  }
}
