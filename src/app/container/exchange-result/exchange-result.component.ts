import { Component, OnInit } from '@angular/core';

import { Router } from '@angular/router';
import { Cookie } from 'ng2-cookies/ng2-cookies';

@Component({
  selector: 'app-exchange-result',
  templateUrl: './exchange-result.component.html',
  styleUrls: ['./exchange-result.component.css']
})
export class ExchangeResultComponent implements OnInit {

  public childId: String = '';
  public point: any = 0;
  public pointCost: any = 0;
  public storeId: String = '';
  public time: String = '';

  constructor(
    private router: Router,
  ) { }

  ngOnInit() {
    this.iniCookie();
  }

  /**
   * 載入 cookie
   */
  public iniCookie() {

    const exchangeCookie = JSON.parse(Cookie.get('exchangeCookie'));
    if (exchangeCookie) {
      this.childId = exchangeCookie.recordchild;
      this.point = exchangeCookie.recordpoint;
      this.pointCost = exchangeCookie.recordcost;
      this.storeId = exchangeCookie.recordstore;
      this.time = exchangeCookie.recordtime;
      Cookie.delete('childCookie','/');
      Cookie.delete('exchangeCookie','/');
    } else {
      this.router.navigate(['/error']);
    }

  }
}
