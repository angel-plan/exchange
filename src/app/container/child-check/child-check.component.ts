import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Cookie } from 'ng2-cookies/ng2-cookies';
import * as moment from 'moment';
import * as swal from 'sweetalert';
import { StoreService } from '../../service/store/store.service';


@Component({
  selector: 'app-child-check',
  templateUrl: './child-check.component.html',
  styleUrls: ['./child-check.component.css'],
  providers: [StoreService]
})
export class ChildCheckComponent implements OnInit {

  public childId: String = '';
  public childName: String = '';
  public point = 0;
  public storeId: String = '';
  public storeName: String = '';
  public isDebug: Boolean = false;
  public childData: any;
  public storeData: any;
  constructor(
    private router: Router,
    private storeService: StoreService
  ) { }

  ngOnInit() {
    const childCookie = Cookie.get('childCookie');
    const storeCookie = Cookie.get('userCookie');
    if (childCookie && storeCookie) {
      this.getStoreInfo();
    } else {
      this.router.navigate(['/error']);
    }
  }

  public async getStoreInfo() {
    await this.storeService.storeUserInfo().subscribe(result => {
      this.storeData = result[0];
      this.getChildUserInfo();
    })
  }

  public async getChildUserInfo() {
    await this.storeService.childUserInfo().subscribe(result => {
      this.childData = result[0];
      this.childId = this.childData.username;
      this.childName = this.childData.name;
      this.point = this.childData.point;
      this.storeId = this.storeData.username;
      this.storeName = this.storeData.name;
    })
  }

  /**
   * 提交表單資料
   */
  public async chkCheck() {
    const body = {
      recordchild: this.childId,
      recordstore: this.storeId,
      recordpoint: this.point,
      recordtime: moment().format('YYYY-MM-DD HH:mm:ss'),
      recordcost: 20 // 扣除點數要討論
    };
    if ((body.recordpoint - body.recordcost) < 0) {
      swal(`兌換失敗`, `點數不足`, `error`);
      Cookie.delete('childCookie', '/');
      this.router.navigate(['/storelogin']);
    } else {
      const child = this.childData;
      child.point = body.recordpoint - body.recordcost;
      const body2= {
        point: child.point,
        username: child.username
      }
      await this.storeService.exchangeUpdate(body2).subscribe(
        result => {
          console.log(result);
          console.log('update success');
        });
      body.recordpoint = body.recordpoint - body.recordcost;
      await this.storeService.exchangeAdd(body).subscribe(
        result => {
          swal(`兌換成功囉！`, `謝謝「${this.storeName}」！\n\n請用餐完畢後\n別忘了清理桌面唷！`, 'success');
          Cookie.set('exchangeCookie', JSON.stringify(body), 36500, '/');
          this.router.navigate(['/exchangeresult']);
        });
    }
  }

}

