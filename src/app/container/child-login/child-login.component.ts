import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { ChildService } from '../../service/child/child.service';
import { Cookie } from 'ng2-cookies/ng2-cookies';
import { SwalComponent } from '@toverux/ngsweetalert2';
import { StoreService } from '../../service/store/store.service';

@Component({
  selector: 'app-child-login',
  templateUrl: './child-login.component.html',
  styleUrls: ['./child-login.component.css'],
  providers: [StoreService]
})
export class ChildLoginComponent implements OnInit {
  @ViewChild('dialogError') private swalDialogError: SwalComponent;
  public childId: String = '';
  public childPwd: String = '';
  public storeName: String = '';

  public reslut: any = null;

  constructor(
    private router: Router,
    private storeService: StoreService
  ) { }

  ngOnInit() {
    this.checkUrl();
  }

  /**
   * 檢查網址參數是否正確
   */
  public async checkUrl() {

    this.childId = Cookie.get('childCookie');
    if (!this.childId) {
      this.router.navigate(['/error']);
    }
    await this.storeService.storeUserInfo()
      .subscribe(result => {
        if (result) {
          this.storeName = result[0].name;
        } else {
          this.router.navigate(['/error']);
        }
      })

  }

  /**
   * 提交表單資料
   */
  public async childCheck() {
    const body = {
      userId: this.childId,
      userPwd: this.childPwd
    };
    await this.childLogin(body);
  }

  /**
   * 登入
   * @param body 表單參數
   */
  public async childLogin(body: Object) {
    return await this.storeService.Login(body)
      .subscribe(
        result => {
          if (result.token) {
            Cookie.delete('childCookie');
            Cookie.set('childCookie', result.token, 36500, '/');
            this.router.navigate(['/childcheck'])
          } else {
            this.swalDialogError.show();
          }
          this.reslut = result[0];
        });
  }

}
