import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { Cookie } from 'ng2-cookies/ng2-cookies';
import { StoreService } from '../../service/store/store.service';

@Component({
  selector: 'app-qrcode',
  templateUrl: './qrcode.component.html',
  styleUrls: ['./qrcode.component.css'],
  providers: [StoreService]
})
export class QrcodeComponent implements OnInit {

  constructor(
    private router: Router,
    private storeService: StoreService
  ) { }

  ngOnInit() {
    this.checkUrl();
  }

  /**
   * http://localhost:4202/qrcode?childid=jason123
   * 檢查網址是否來自 QRcode 的標準格式
   */
  public checkUrl() {
    const id = this.router.parseUrl(this.router.url).queryParams['childid'];
    if (id) {
      Cookie.set('childCookie', id, 36500, '/');
      this.router.navigate(['/storelogin']);
    } else {
      this.router.navigate(['/error']);
    }
  }

}
