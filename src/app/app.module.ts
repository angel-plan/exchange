import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HashLocationStrategy, LocationStrategy } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { HttpModule, JsonpModule } from '@angular/http';
import { AppRoutingModule } from './app-routing.module';

import { enableProdMode } from '@angular/core';

/* component */
import { AppComponent } from './app.component';
import { FooterComponent } from './container/footer/footer.component';
import { NavComponent } from './container/nav/nav.component';
import { QrcodeComponent } from './container/qrcode/qrcode.component';
import { StoreLoginComponent } from './container/store-login/store-login.component';
import { ChildLoginComponent } from './container/child-login/child-login.component';
import { ChildCheckComponent } from './container/child-check/child-check.component';
import { ExchangeResultComponent } from './container/exchange-result/exchange-result.component';
import { ErrorComponent } from './container/error/error.component';
import { SweetAlert2Module } from '@toverux/ngsweetalert2';
import { APP_BASE_HREF } from '@angular/common';

/* plugin */

enableProdMode();

@NgModule({
  declarations: [
    AppComponent,
    FooterComponent,
    NavComponent,
    StoreLoginComponent,
    ChildLoginComponent,
    ErrorComponent,
    ChildCheckComponent,
    ExchangeResultComponent,
    QrcodeComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    JsonpModule,
    AppRoutingModule,
    SweetAlert2Module.forRoot()
  ],
  providers: [{ provide: LocationStrategy, useClass: HashLocationStrategy }],
  bootstrap: [AppComponent]
})
export class AppModule { }
