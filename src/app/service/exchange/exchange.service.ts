import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';

@Injectable()
export class ExchangeService {

  private api: any = '/api/record/add';
  private api2: any = '/api/user/update';
  constructor(private http: Http) { }

  public exchangeAdd(body: Object) {

    const headers = new Headers({ 'Content-Type': 'application/json' });
    const options = new RequestOptions({ headers: headers });

    return this.http.post(this.api, body)
      .map((res) => {
        return res.json() || {};
      });
  }

  public exchangeUpdate(formdata: FormData) {
    return this.http.post(this.api2, formdata)
      .map((res) => {
        return res.json() || {};
      });
  }
}
