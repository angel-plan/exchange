import { Injectable, Inject } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Cookie } from 'ng2-cookies/ng2-cookies';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';

@Injectable()
export class StoreService {

  private api: any = '/api/user/login';
  constructor(@Inject(Http) private http: Http) { }

  /**
   * Token打包
   *
   * @memberof UserService
   */
  public storepackToken() {
    const headers = new Headers({ 'Content-Type': 'application/json' });
    headers.append('Authorization', 'Bearer ' + Cookie.get('userCookie'));
    const options = new RequestOptions({ headers: headers });
    return options;
  }

  public childpackToken() {
    const headers = new Headers({ 'Content-Type': 'application/json' });
    headers.append('Authorization', 'Bearer ' + Cookie.get('childCookie'));
    const options = new RequestOptions({ headers: headers });
    return options;
  }

  public Login(body: Object) {

    return this.http.post(this.api, body)
      .map((res) => {
        return res.json() || {}
      });
  }

  public storeUserInfo() {
    return this.http.get('/api/user/userinfo', this.storepackToken())
      .map((res) => {
        if (res.json().length !== undefined && res.json() !== false) {
          return res.json() || {}
        } else {
          return false;
        }
      });
  }

  public childUserInfo() {
    return this.http.get('/api/user/userinfo', this.childpackToken())
      .map((res) => {
        if (res.json().length !== undefined && res.json() !== false) {
          return res.json() || {}
        } else {
          return false;
        }
      });
  }

  public exchangeAdd(body: Object) {

    return this.http.post('/api/record/create', body, this.storepackToken())
      .map((res) => {
        return res.json() || {};
      });
  }

  public exchangeUpdate(body: Object) {
    return this.http.post('/api/user/update', body, this.childpackToken())
      .map((res) => {
        return res.json() || {};
      });
  }
}
