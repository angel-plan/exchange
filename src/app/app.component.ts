import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  constructor(
    private router: Router,
  ) { }

  ngOnInit() {

    const parser = document.createElement('a');
    parser.href = document.URL;

    const search = parser.search.split('=');
    if (
      parser.pathname === '/qrcode' &&
      search[0] === '?childid' &&
      search[1] !== '' &&
      search[1] !== undefined) {
      this.router.navigate([`qrcode`], { queryParams: { 'childid': search[1] } });
    }
  }

}
